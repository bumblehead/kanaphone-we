// Filename: build.js
// Timestamp: 2018.01.14-15:08:05 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import fs from 'fs'
import url from 'url'
import path from 'path'
import { execSync } from 'child_process'
import scroungejs from 'scroungejs'

const __filename = new url.URL('', import.meta.url).pathname
const __dirname = __filename.replace(/[/\\][^/\\]*?$/, '')

// const buildScaledPng = (w, h) => {
//   const iconsvg = __dirname + '/src/icons/kanaphone.svg';
//   const iconpng = __dirname + `/src/icons/kanaphone-${w}x${h}.png`;
//   exec(`inkscape -w ${w} -h ${h} ${iconsvg} --export-filename ${iconpng}`,
//   (err, stdout) => {
//     console.log('created png icon', { err, stdout });
//   });
// };

const cp_rf = (from, to) => {
  from = path.join(__dirname, from)
  to = path.join(__dirname, to)

  console.log(`[...] exec: cp -rf ${from} ${to}`)
  execSync(`mkdir -p ${to}`)
  execSync(`cp -rf ${from} ${to}`)
}

const writeJSON = (obj, to) => {
  console.log(`[...] write: ${to}`)
  fs.writeFileSync(to, JSON.stringify(obj, null, '  '))
}

(async () => {
  await scroungejs({
    version: process.env.npm_package_version,
    inputpath: './src',
    outputpath: './build',
    publicpath: './',
    isuidfilenames: false,
    iscompress: false,
    isconcat: false,
    iswatch: false,
    deploytype: 'module',
    basepagein: './src/kpwe_choose.tpl.html',
    basepage: './build/kpwe_choose.html',
    treearr: [
      'kpwe_content.js',
      'kpwe.js',
      'kpwe.css',
      'kpwe_choose.js',
      'kpwe_choose.css',
      'kpwe_background.js'
    ]
  })

  cp_rf('src/icons/*svg', 'build/icons/')

  if (process.env.NODE_ENV === 'production') {
    cp_rf('build', 'stage')
    cp_rf('src/icons/*png', 'stage')

    const manifest = JSON.parse(
      fs.readFileSync(`${__dirname}/manifest.json`))
    
    manifest.version = String(process.env.npm_package_version)

    writeJSON(manifest, 'stage/manifest.json')
    // eslint-disable-next-line max-len
    execSync(`cd stage && zip -r ../kanaphone-we.firefox.zip manifest.json build`)
    writeJSON(Object.assign(manifest, {
      icons: {
        16: 'kanaphone-16x16.png',
        48: 'kanaphone-48x48.png',
        96: 'kanaphone-96x96.png',
        128: 'kanaphone-128x128.png'
      },
      background: {
        type: manifest.background.type,
        service_worker: manifest.background.scripts[0]
      },
      action: {
        ...manifest.action,
        default_icon: 'kanaphone-32x32.png'
      }
    }), 'stage/manifest.json')
    // eslint-disable-next-line max-len
    execSync(`cd stage && zip -r ../kanaphone-we.chrome.zip manifest.json build *png`)
  }
})()
