import test from 'node:test'
import assert from 'node:assert/strict'
import esmock from 'esmock'
import jsdom from 'jsdom'

const { JSDOM } = jsdom

const mockcfg = {
  isfurigana: false,
  ishiragana: true,
  iskatakana: false,
  disabledhostnames: [],
  chardistmax: 20
}

const text = `Entangled states are famously fragile: in most cases even a
tiny disturbance will undo the entanglement. For this reason, current quantum
technologies take great pains to isolate the microscopic systems they work
with, and typically operate at temperatures close to absolute zero. The
ICFO team, in contrast, heated a collection of atoms to 450 Kelvin, millions
of times hotter than most atoms used for quantum technology. Moreover, the
individual atoms were anything but isolated; they collided with each other
every few microseconds, and each collision set their electrons spinning in
random directions.`

const buildDOM = htmlStr => {
  const dom = new JSDOM(`<!DOCTYPE html><body>${htmlStr}</body>`)

  global.window = dom.window
  global.document = dom.window.document
  global.MutationObserver = dom.window.MutationObserver
}

test.beforeEach(() => buildDOM(`<p>${text}</p>`))

test('should initialize cfg', async () => {
  const state = {}

  await esmock('../src/kpwe.js', {
    '../node_modules/kanaphone/src/kp_cfg.js': cfg => cfg,
    '../node_modules/kanaphone/src/kp_reduce.js': (cfg, str, fn) => {
      return fn(cfg, str.slice(20, 30), [null, 'ふ', 'り'])
    },
    '../src/kpwe_cfg.js': () => {},
    '../src/kpwe_backgroundmsg.js': {
      getcfg: () => {
        state.getcfg = true
      },
      lsn: () => {}
    }
  })

  assert.ok(state.getcfg)
  assert.ok(true)
})

test('should update the document', async () => {
  const state = {}
  await esmock('../src/kpwe.js', {
    '../node_modules/kanaphone/src/kp_cfg.js': cfg => cfg,
    '../node_modules/kanaphone/src/kp_reduce.js': (cfg, str, fn, prev) => {
      return fn(prev, str.slice(0, 8), [null, 'ふ', 'り'])
    },    
    '../src/kpwe_backgroundmsg.js': {
      getcfg: fn => fn(null, mockcfg),
      lsn: () => {
        state.lsn = true
      }
    }
  })

  assert.strictEqual(
    global.document.getElementsByTagName('p')[0].textContent, `ふり`)
})

test('should not modify pre tag node hierarchy', async () => {
  const html = `
    <pre>
      <code>
        <span>1</span>
        <span>2</span>
        <span>3</span>
        <span>4</span>
        <span>5</span>
        <span>6</span>
        <span>7</span>
        <span>8</span>
        <span>9</span>
        <span>10</span>
        <span>11</span>
        <span>12</span>
        <span>13</span>
        <span>14</span>
        <span>15</span>
        <span>16</span>
        <span>17</span>
        <span>18</span>
        <span>19</span>
        <span>20</span>
        <span>21</span>
        <span>22</span>
        <span>23</span>
        <span>24</span>
      </code>
      <code>
        class A():
          pass

        class B(A):
          def accepts(type):
            return type == "image"

        class C(A):
          def accepts(type):
            return type == "audio"

        def get_filetype_class(type):
          for ft_class in A.__subclasses__():
            if ft_class.accepts(type):
              return ft_class
          raise LookupError(f'Unknown filetype "{type}".')

        try:
          print(get_filetype_class("image"))
          print(get_filetype_class("audio"))
          print(get_filetype_class("error lmao"))
        except LookupError as e:
          print(e)
      </code>
    </pre>`

  buildDOM(html)

  const state = { replaced: false }
  await esmock('../src/kpwe.js', {
    '../node_modules/kanaphone/src/kp_cfg.js': cfg => cfg,
    '../node_modules/kanaphone/src/kp_reduce.js': (cfg, str, fn, prev) => {
      state.replaced = true

      return fn(prev, str.slice(0, 8), [null, 'ふ', 'り'])
    },    
    '../src/kpwe_backgroundmsg.js': {
      getcfg: fn => fn(null, mockcfg),
      lsn: () => {
        state.lsn = true
      }
    }
  })

  setTimeout(() => {
    assert.strictEqual(state.replaced, false)
  }, 1000)
})
