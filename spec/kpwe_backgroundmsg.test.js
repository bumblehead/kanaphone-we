import test from 'node:test'
import assert from 'node:assert/strict'
import esmock from 'esmock'

const requiremocked_kpwe_backgroundmsg = async (cfg = {}) => {
  const addListener = { addListener: () => {} }
  return esmock('../src/kpwe_backgroundmsg.js', {
    '../src/kpwe_webapi.js': {
      webapi: {
        browserAction: cfg.browserAction || { setIcon: () => {} }
      },
      tabs: {
        onActivated: addListener,
        onUpdated: addListener,
        ...cfg.tabs
      },
      runtime: {
        onMessage: addListener,
        sendMessage: (/*spec, fn*/) => {},
        ...cfg.runtime
      }
    }
  })
}

test('getcfg should request getcfg from browser.runtime', async () => {
  const kpwe_backgroundmsg = await requiremocked_kpwe_backgroundmsg({
    runtime: {
      sendMessage: (spec/*, fn*/) => {
        assert.deepStrictEqual({
          method: 'getcfg'
        }, spec)
      }
    }
  })

  kpwe_backgroundmsg.getcfg(() => {
    assert.ok(true)
  })
})

test('setcfg should request setcfg from browser.runtime', async () => {
  const kpwe_backgroundmsg = await requiremocked_kpwe_backgroundmsg({
    runtime: {
      sendMessage: (spec/*, fn*/) => {
        assert.deepStrictEqual({
          method: 'setcfg',
          cfg: { foo: 'bar' }
        }, spec)
      }
    }
  })

  kpwe_backgroundmsg.setcfg({ foo: 'bar' }, () => {
    assert.ok(true)
  })
})

test('lsn should call browser.runtime.onMessage.addListener', async () => {
  const state = {}
  const kpwe_backgroundmsg = await requiremocked_kpwe_backgroundmsg({
    runtime: {
      onMessage: {
        addListener: () => {
          state.called = true
        }
      }
    }
  })

  kpwe_backgroundmsg.lsn(() => {})
  assert.ok(state.called)
})
