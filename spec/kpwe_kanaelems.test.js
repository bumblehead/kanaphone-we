import test from 'node:test'
import assert from 'node:assert/strict'
import jsdom from 'jsdom'
import kpwe_kanaelems from '../src/kpwe_kanaelems.js'

const { JSDOM } = jsdom

test('createkanaelem, should return a kana node hierarch`', () => {
  const dom = new JSDOM('<!DOCTYPE html>')
  const node = kpwe_kanaelems
    .createkanaelem('あ', 'a', 'A', {}, dom.window.document)

  assert.strictEqual(
    node.outerHTML,
    // eslint-disable-next-line max-len
    '<kana class="isfurigana-false" title="A, (a)"><ruby>あ<rt>a</rt></ruby></kana>'
  )
})

test('iskanaelem, should return true if kana elem`', () => {
  const dom = new JSDOM('<!DOCTYPE html>')
  const node = kpwe_kanaelems
    .createkanaelem('あ', 'a', 'A', {}, dom.window.document)

  node.setAttribute('kana', 1)
  assert.ok(kpwe_kanaelems.iskanaelem(node))
})
