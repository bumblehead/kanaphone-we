import test from 'node:test'
import assert from 'node:assert/strict'
import kpwe_cfg from '../src/kpwe_cfg.js'

test('isorigindisabled, should return true', () => {
  assert.ok(
    kpwe_cfg.isorigindisabled({
      disabledhostnames: ['bumblehead.com']
    }, 'bumblehead.com')
  )
})

test('isorigindisabled, should return false', () => {
  assert.ok(
    !kpwe_cfg.isorigindisabled({
      disabledhostnames: []
    }, 'bumblehead.com')
  )
})

test('isKane, should return boolean', () => {
  assert.ok(kpwe_cfg.isKana({ iskatakana: true }))
  assert.ok(!kpwe_cfg.isKana({ iskatakana: false }))
})

test('shouldKana, should return boolean', () => {
  assert.ok(!kpwe_cfg.shouldKana({
    iskatakana: true,
    disabledhostnames: ['bumblehead.com']
  }, 'bumblehead.com'))

  assert.ok(!kpwe_cfg.shouldKana({
    iskatakana: false,
    disabledhostnames: []
  }, 'bumblehead.com'))

  assert.ok(kpwe_cfg.shouldKana({
    iskatakana: true,
    disabledhostnames: []
  }, 'bumblehead.com'))
})

test('ischanged, should return boolean', () => {
  assert.ok(kpwe_cfg.ischanged({
    iskatakana: false
  }, {
    iskatakana: true
  }, 'bumblehead.com'))

  assert.ok(!kpwe_cfg.ischanged({
    iskatakana: true
  }, {
    iskatakana: true
  }, 'bumblehead.com'))
})
