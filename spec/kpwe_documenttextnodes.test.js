import test from 'node:test'
import assert from 'node:assert/strict'
import jsdom from 'jsdom'
import kpwe_documenttextnodes from '../src/kpwe_documenttextnodes.js'
const { JSDOM } = jsdom

const createDOM = htmlStr => (
  new JSDOM(`<!DOCTYPE html><body>${htmlStr}</body>`))

test('should return a single textNode`', () => {
  const dom = createDOM('<p>Hello world</p>')
  const { window } = dom
  const nodes = kpwe_documenttextnodes(window.document.body, [], window)

  assert.strictEqual(nodes.length, 1)
  assert.strictEqual(nodes[0].textContent, 'Hello world')
})

test('should not return textNode inside presentation node`', () => {
  const html = `
    <pre>
      <code>
        <span>1</span>
        <span>2</span>
        <span>3</span>
        <span>4</span>
        <span>5</span>
        <span>6</span>
        <span>7</span>
        <span>8</span>
        <span>9</span>
        <span>10</span>
        <span>11</span>
        <span>12</span>
        <span>13</span>
        <span>14</span>
        <span>15</span>
        <span>16</span>
        <span>17</span>
        <span>18</span>
        <span>19</span>
        <span>20</span>
        <span>21</span>
        <span>22</span>
        <span>23</span>
        <span>24</span>
      </code>
      <code>
        class A():
          pass

        class B(A):
          def accepts(type):
            return type == "image"

        class C(A):
          def accepts(type):
            return type == "audio"

        def get_filetype_class(type):
          for ft_class in A.__subclasses__():
            if ft_class.accepts(type):
              return ft_class
          raise LookupError(f'Unknown filetype "{type}".')

        try:
          print(get_filetype_class("image"))
          print(get_filetype_class("audio"))
          print(get_filetype_class("error lmao"))
        except LookupError as e:
          print(e)
      </code>
    </pre>`

  const dom = createDOM(html)  
  const { window } = dom
  const nodes = kpwe_documenttextnodes(window.document.body, [], window)

  assert.strictEqual(nodes.length, 0)
})
