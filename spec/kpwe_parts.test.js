import test from 'node:test'
import assert from 'node:assert/strict'

import kpwe_parts from '../src/kpwe_parts.js'

test('basic test should return `true` from `"true"`', () => {
  const finarr = []
  const bgnarr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']

  kpwe_parts(bgnarr, {
    pos: 0,
    maxparts: 6,
    activeparts: 0,
    completedparts: 0
  }, s => finarr.push(s), () => {
    assert.deepStrictEqual(bgnarr, finarr)
  })
})
