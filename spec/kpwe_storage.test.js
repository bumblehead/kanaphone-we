import test from 'node:test'
import assert from 'node:assert/strict'
import esmock from 'esmock'

test('set, should set cfg values to localStorage', async () => {
  const store = []
  const { kpwe_storageset } = await esmock('../src/kpwe_storage.js', {
    '../src/kpwe_webapi.js': {
      storage: {
        local: {
          set: o => store.push(o)
        }
      }
    }
  })

  await kpwe_storageset({
    isfurigana: true,
    iskatakana: true,
    ishiragana: false,
    disabledhostnames: ['bumblehead.com'],
    chardistmax: 20
  })

  assert.deepStrictEqual(store, [{
    disabledhostnames: ['bumblehead.com'],
    isfurigana: true,
    iskatakana: true,
    ishiragana: false,
    chardistmax: 20
  }])
})

test('get, should get cfg values from localStorage', async () => {
  const { kpwe_storageget } = await esmock('../src/kpwe_storage.js', {
    '../src/kpwe_webapi.js': {
      storage: {
        local: {
          get: () => ({
            disabledhostnames: ['bumblehead.com'],
            isfurigana: true,
            iskatakana: true,
            ishiragana: false,
            chardistmax: 20
          })
        }
      }
    }
  })
  
  assert.deepStrictEqual(await kpwe_storageget(), {
    disabledhostnames: ['bumblehead.com'],
    isfurigana: true,
    iskatakana: true,
    ishiragana: false,
    chardistmax: 20
  })
})
