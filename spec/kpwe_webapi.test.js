import test from 'node:test'
import assert from 'node:assert/strict'
import esmock from 'esmock'

test('ischrome should return false', async () => {
  const kpwe_webapi = await esmock('../src/kpwe_webapi.js')
  
  assert.ok(!kpwe_webapi.ischrome)
  assert.ok(!kpwe_webapi.ismoz)
})

test('ischrome should return true', async () => {
  const kpwe_webapi = await esmock('../src/kpwe_webapi.js', {
    import: {
      chrome: {}
    }
  })

  assert.ok(kpwe_webapi.ischrome)
  assert.ok(typeof kpwe_webapi.tabs.query === 'function')
  assert.ok(typeof kpwe_webapi.tabs.sendMessage === 'function')
  assert.ok(typeof kpwe_webapi.runtime.onMessage.addListener === 'function')
  assert.ok(typeof kpwe_webapi.runtime.sendMessage === 'function')
})

test('ismoz should return true', async () => {
  const kpwe_webapi = await esmock('../src/kpwe_webapi.js', {
    import: {
      browser: {}
    }
  })

  assert.ok(kpwe_webapi.ismoz)
  assert.ok(typeof kpwe_webapi.tabs.query === 'function')
  assert.ok(typeof kpwe_webapi.tabs.sendMessage === 'function')
  assert.ok(typeof kpwe_webapi.runtime.onMessage.addListener === 'function')
  assert.ok(typeof kpwe_webapi.runtime.sendMessage === 'function')
})
