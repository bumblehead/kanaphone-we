import test from 'node:test'
import assert from 'node:assert/strict'
import esmock from 'esmock'

const mocktab = { id: 'mocktabid' }

test('getactivetab, should return active tab', async () => {
  const params = []
  const kpwe_activetabmsg = await esmock('../src/kpwe_activetabmsg.js', {
    '../src/kpwe_webapi.js': {
      tabs: {
        query: (opts, fn) => params.push([opts, fn])
      }
    }
  })

  kpwe_activetabmsg.getactivetab(() => {})

  assert.deepStrictEqual(params[0][0], {
    active: true,
    currentWindow: true
  })
})

test('activetablog, should send a log message to the active tab', async () => {
  const params = []
  const kpwe_activetabmsg = await esmock('../src/kpwe_activetabmsg.js', {
    '../src/kpwe_webapi.js': {
      tabs: {
        query: (opts, fn) => fn(null, [mocktab]),
        sendMessage: (id, opts) => params.push([id, opts])
      }
    }
  })

  kpwe_activetabmsg.activetablog('message')

  assert.deepStrictEqual(params[0], [mocktab.id, {
    method: 'log', msg: 'message'
  }])
})

test('activetabactivated, sends activated message to active tab', async () => {
  const params = []
  const kpwe_activetabmsg = await esmock('../src/kpwe_activetabmsg.js', {
    '../src/kpwe_webapi.js': {
      tabs: {
        sendMessage: (id, opts) => params.push([id, opts])
      }
    }
  })

  kpwe_activetabmsg.activetabactivated('message', 'tabid')

  assert.deepStrictEqual(params[0], ['tabid', {
    method: 'activated', cfg: 'message'
  }])
})

test('activetabapplycfg, should send cfg to the active tab', async () => {
  const params = []
  const kpwe_activetabmsg = await esmock('../src/kpwe_activetabmsg.js', {
    '../src/kpwe_webapi.js': {
      tabs: {
        query: (opts, fn) => fn(null, [mocktab]),
        sendMessage: (id, opts) => params.push([id, opts])
      }
    }
  })

  kpwe_activetabmsg.activetabapplycfg('cfg')

  assert.deepStrictEqual(params[0], [mocktab.id, {
    method: 'applycfg', cfg: 'cfg'
  }])
})
