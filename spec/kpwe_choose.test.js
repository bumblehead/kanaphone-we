import test from 'node:test'
import assert from 'node:assert/strict'
import esmock from 'esmock'
import jsdom from 'jsdom'
import fs from 'fs'

const kpwe_choosehtml = fs.readFileSync('./src/kpwe_choose.tpl.html')

const { JSDOM } = jsdom

const mocktab = {
  id: 'mocktabid',
  url: 'https://mockurl.com'
}

const mockcfg = {
  isfurigana: false,
  ishiragana: true,
  iskatakana: false,
  disabledhostnames: [],
  chardistmax: 20
}

const gEBId = str => global.document.getElementById(str)

test.beforeEach(() => {
  const dom = new JSDOM(kpwe_choosehtml)

  global.window = dom.window
  global.document = dom.window.document
})

test('should initialise', async () => {
  const state = { setcfgcalled: 0 }
  await esmock('../src/kpwe_choose.js', {
    '../src/kpwe_backgroundmsg.js': {
      getcfg: fn => fn(null, {
        ...mockcfg,
        isfurigana: true,
        ishiragana: true,
        iskatakana: true
      }),
      setcfg: cfg => {
        assert.ok(typeof cfg === 'object')

        if (++state.setcfgcalled > 2)
          assert.ok(true)
      }
    },
    '../src/kpwe_activetabmsg.js': {
      activetablog: () => {},
      getactivetab: fn => {
        fn(null, mocktab)

        assert.ok(gEBId('isfurigana').checked)
        assert.ok(gEBId('iskatakana').checked)
        assert.ok(gEBId('ishiragana').checked)

        gEBId('iskatakana').dispatchEvent(
          new global.window.Event('change', { bubbles: true }))

        gEBId('isorigindisabled').dispatchEvent(
          new global.window.Event('change', { bubbles: true }))

        gEBId('chardistmax').dispatchEvent(
          new global.window.Event('change', { bubbles: true }))
      }
    }    
  })
})

test('setkanarange should apply value to range', async () => {
  const kpwe_choose = await esmock('../src/kpwe_choose.js', {
    '../src/kpwe_backgroundmsg.js': {
      getcfg: () => {}
    }    
  })

  kpwe_choose.setkanarange(mockcfg, 'mockhost.com')

  assert.strictEqual(gEBId('chardistmax').value, '20')
  assert.strictEqual(gEBId('chardistmax').title, '頻度 50%')
})

test('isdisabledhost should return true for disabled host', async () => {
  const kpwe_choose = await esmock('../src/kpwe_choose.js', {
    '../src/kpwe_backgroundmsg.js': {
      getcfg: () => {}
    }    
  })

  assert.ok(kpwe_choose.isdisabledhost({
    disabledhostnames: ['testhostname.com']
  }, 'testhostname.com'))
})

test('isdisabledhost should return false for enabled host', async () => {
  const kpwe_choose = await esmock('../src/kpwe_choose.js', {
    '../src/kpwe_backgroundmsg.js': {
      getcfg: () => {}
    }    
  })

  assert.ok(!kpwe_choose.isdisabledhost({
    disabledhostnames: ['testhostname.com']
  }, 'testhostname-other.com'))
})

test('setKanaButtons should disable kana buttons', async () => {
  const kpwe_choose = await esmock('../src/kpwe_choose.js', {
    '../src/kpwe_backgroundmsg.js': {
      getcfg: () => {}
    }    
  })

  kpwe_choose.setKanaButtons({
    disabledhostnames: ['testhostname.com']
  }, 'testhostname.com')

  assert.ok(gEBId('iskatakana').disabled)
  assert.ok(!gEBId('iskatakana').checked)
  assert.ok(gEBId('ishiragana').disabled)
  assert.ok(!gEBId('ishiragana').checked)
})

