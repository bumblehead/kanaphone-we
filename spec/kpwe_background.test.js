import test from 'node:test'
import assert from 'node:assert/strict'
import esmock from 'esmock'

const mocktab = {
  id: 'mocktabid',
  url: 'mockhostname.com'
}

const requiremocked_kpwe_background = async (cfg = {}) => {
  const addListener = { addListener: () => {} }
  return esmock('../src/kpwe_background.js', {
    '../src/kpwe_activetabmsg.js': {
      getactivetab: cfg.getactivetab || (fn => fn(null, mocktab))
    },
    '../src/kpwe_webapi.js': {
      webapi: {
        action: {
          setIcon: () => {},
          onClicked: addListener,
          ...cfg.action
        }
      },
      tabs: {
        onActivated: addListener,
        onUpdated: addListener,
        ...cfg.tabs
      },
      runtime: {
        onMessage: addListener,
        ...cfg.runtime
      }
    }
  })
}

test('should attach event and message listeners', async () => {
  const addedListeners = []
  const addListener = str => fn => addedListeners.push([str, fn])
  await requiremocked_kpwe_background({
    tabs: {
      onActivated: {
        addListener: addListener('onActivated')
      },
      onUpdated: {
        addListener: addListener('onUpdated')
      }
    },
    runtime: {
      onMessage: {
        addListener: addListener('onMessage')
      }
    }    
  })

  assert.deepStrictEqual(addedListeners.map(([str]) => str).sort(), [
    'onMessage',
    'onActivated',
    'onUpdated'
  ].sort())
})

test('urlhostname should return the hostname from a url', async () => {
  const kpwe_background = await requiremocked_kpwe_background()

  assert.strictEqual(
    kpwe_background.urlhostname('https://myhostname.com'), 'myhostname.com')
})

test('urlhostname should return null when hostname not found', async () => {
  const kpwe_background = await requiremocked_kpwe_background()

  assert.strictEqual(
    kpwe_background.urlhostname('about:config'), null)
})

test('setIcon calls browser.action.setIcon in firefox', async () => {
  const state = { setIcon: false }
  const kpwe_background = await requiremocked_kpwe_background({
    action: {
      setIcon: () => {
        state.setIcon = true
      }
    },
    getactivetab: fn => {
      global.navigator = { userAgent: 'firefox' }
      fn(null, mocktab)
      assert.ok(state.setIcon)
    }
  })

  kpwe_background.setIcon({
    disabledhostnames: []
  })
})
