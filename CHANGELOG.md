2.8.0
 * bug, un-expected text cols inside display:flex;flex-direction:column

2.6.9
 * bug, update new active tab on hostname configuration change

2.6.8
 * bug, kana only applied to first observed mutation

2.6.7
 * feature, middle-mouse-click extension icon as quick toggle
 * bug, pre tags not filtered correctly
