const createrubyelem = (kana, phoneme, doc = document) => {
  const rubyelem = doc.createElement('ruby')
  const rtelem = doc.createElement('rt')

  rubyelem.textContent = kana
  rtelem.textContent = phoneme

  rubyelem.appendChild(rtelem)
  return rubyelem
}

const createkanaelem = (kana, phoneme, substr, cfg = {}, doc = document) => {
  const kanaelem = doc.createElement('kana')
  const rubyelem = createrubyelem(kana, phoneme, doc)

  kanaelem.className = `isfurigana-${Boolean(cfg.isfurigana)}`
  kanaelem.setAttribute(
    'title', substr === phoneme
      ? phoneme : `${substr}, (${phoneme})`)

  kanaelem.appendChild(rubyelem)
  return kanaelem
}

const createspanelem = (doc = document, elem) => (
  elem = doc.createElement('span'),
  elem.className = `kanaphone-span`,
  elem)

const iskanaelem = node => node && (
  /kana/i.test(node.nodeName) || node.hasAttribute('kana'))

const updatekanaelem = (kanaelem, cfg) => (
  kanaelem.className = `isfurigana-${Boolean(cfg.isfurigana)}`,
  kanaelem)

export default {
  createrubyelem,
  createkanaelem,
  createspanelem,
  iskanaelem,
  updatekanaelem
}
