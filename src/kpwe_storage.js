import webapi from './kpwe_webapi.js'

const kpwe_storageget = async () => {
  const cfg = await webapi.storage.local.get({
    disabledhostnames: [],
    isfurigana: true,
    iskatakana: true,
    ishiragana: true,
    chardistmax: 16
  })

  return cfg
}

const kpwe_storageset = async cfg => {
  await webapi.storage.local.set({
    disabledhostnames: cfg.disabledhostnames,
    isfurigana: cfg.isfurigana,
    iskatakana: cfg.iskatakana,
    ishiragana: cfg.ishiragana,
    chardistmax: cfg.chardistmax
  })

  return cfg
}

export {
  kpwe_storageget,
  kpwe_storageset
}
