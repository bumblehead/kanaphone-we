// https://developer.mozilla.org/en-US/docs/Web/HTML/Element
export default (el, arr = [], win = window) => {
  const {
    SHOW_TEXT,
    FILTER_SKIP,
    FILTER_REJECT,
    FILTER_ACCEPT
  } = win.NodeFilter

  // eslint-disable-next-line one-var
  const presentationNodeTags = [
    'script', 'noscript', 'canvas', 'style', 'pre', 'i', 'code'
  ].join(', ')

  // eslint-disable-next-line one-var
  let walk = win.document.createTreeWalker(el, SHOW_TEXT, {
    acceptNode: node => {
      if (node.parentNode.closest(presentationNodeTags))
        return FILTER_REJECT

      return /\w/.test(node.textContent)
        ? FILTER_ACCEPT
        : FILTER_SKIP
    }
  }, false)

  // eslint-disable-next-line one-var
  let node = walk.nextNode()

  while (node) {
    arr.push(node)
    node = walk.nextNode(node)
  }

  return arr
}
