import kpwe_webapi from './kpwe_webapi.js'

const getcfg = fn => (
  kpwe_webapi.runtime.sendMessage({
    method: 'getcfg'
  }, fn))

const setcfg = (cfg, fn) => (
  kpwe_webapi.runtime.sendMessage({
    method: 'setcfg', cfg
  }, typeof fn === 'function' ? fn : () => {}))

const lsn = fn => (
  kpwe_webapi.runtime.onMessage.addListener((req, sender, send) => {
    if (req.method === 'log') {
      send({ logged: req.msg })
    } else {
      fn(req, sender, send)
    }
  }))

export {
  getcfg,
  setcfg,
  lsn
}
