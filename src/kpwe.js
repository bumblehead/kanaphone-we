import kp_cfg from 'kanaphone/src/kp_cfg.js'
import kp_reduce from 'kanaphone/src/kp_reduce.js'
import kpwe_parts from './kpwe_parts.js'
import kpwe_kanaelems from './kpwe_kanaelems.js'
import kpwe_documenttextnodes from './kpwe_documenttextnodes.js'
import kpwe_cfg from './kpwe_cfg.js'

import {
  getcfg,
  lsn
} from './kpwe_backgroundmsg.js'

const {
  iskanaelem,
  updatekanaelem,
  createspanelem
} = kpwe_kanaelems

const getkananodearr = () => (
  Array.from(document.getElementsByTagName('kana')))

const isFlexColumn = elem => {
  const compStyles = window.getComputedStyle(elem)

  return compStyles.getPropertyValue('display') === 'flex'
    && compStyles.getPropertyValue('flex-direction') === 'column'
}

// todo: add comment explaining why this is here
const setAttributeKana = node => Array.from(node.children).map(cnode => {
  if (cnode.nodeType === 1 && !iskanaelem(cnode)) {
    cnode.setAttribute('kana', 1)
  }
})

const NodeReplace = (cfg, node) => {
  let { parentNode } = node,
      str = node.nodeValue

  // build text node fragmenent and replace
  if (/[\w]/.test(str)) {
    const frag = kp_reduce(cfg, str, (frag, substr, [, kana, phoneme]) => {
      const node = kana
        ? kpwe_kanaelems.createkanaelem(kana, phoneme, substr, cfg)
        : document.createTextNode(substr)

      frag.appendChild(node)
      return frag
    }, document.createDocumentFragment())

    // some sites render text content inside display:flex;flex-direction:column
    // and layout breaks into separated lines when <kana> elements are added
    // add a span element first to prevent this
    if (isFlexColumn(parentNode)) {
      /* c8 ignore start */
      const spanElem = createspanelem()

      spanElem.appendChild(frag)
      parentNode.replaceChild(spanElem, node)

      setAttributeKana(spanElem)
      /* c8 ignore end */
    } else {
      parentNode.replaceChild(frag, node)

      setAttributeKana(parentNode)
    }
  }
}

const restoreNode = node => {
  const { parentNode, previousSibling, nextSibling } = node
  const [originalTitle] = node.getAttribute('title').split(',')

  if (previousSibling && previousSibling.nodeType === Node.TEXT_NODE) {
    previousSibling.textContent += originalTitle
    parentNode.removeChild(node)
  } else if (nextSibling && nextSibling.nodeType === Node.TEXT_NODE) {
    nextSibling.textContent += originalTitle
    parentNode.removeChild(node)
  } else {
    // should check previous and next nodes to join thfeaere first
    parentNode.replaceChild(
      document.createTextNode(originalTitle), node)
  }
}

const restoreDocument = (cfg, fn) => kpwe_parts(getkananodearr(), {
  pos: 0,
  maxparts: 6,
  activeparts: 0,
  completedparts: 0
}, node => node.parentNode && restoreNode(node), fn || (() => {}))

const applyKana = (cfg, pnode = document.body, fn) => kpwe_parts(
  kpwe_documenttextnodes(pnode), {
    pos: 0,
    maxparts: 6,
    activeparts: 0,
    completedparts: 0
  }, node => node.parentNode && NodeReplace(cfg, node), fn || (() => {}))

const applyDocument = cfg => restoreDocument(cfg, () => {
  if (kpwe_cfg.shouldKana(cfg)) {
    applyKana(cfg)
  }
})

const applyDocumentFuri = cfg => (
  getkananodearr().map(node => updatekanaelem(node, cfg)))

if (!document.getElementById('kpwe-root')) {
  getcfg((err, cfg) => {
    if (err) return console.error(err)

    cfg = Object.assign(cfg, kp_cfg(cfg))

    lsn((req, sender, send) => {
      // get updates to properties
      if (req.method === 'applycfg') {
        if (kpwe_cfg.ischangedfuri(cfg, req.cfg)) {
          applyDocumentFuri(Object.assign(cfg, req.cfg))
        } else {
          applyDocument(Object.assign(cfg, req.cfg))
        }
      } else if (req.method === 'activated') {
        if (kpwe_cfg.ischanged(cfg, req.cfg)) {
          applyDocument(Object.assign(cfg, req.cfg))
        }
      } else {
        send({})
      }
    })

    if (kpwe_cfg.shouldKana(cfg)) {
      applyKana(cfg)
    }

    let obs = new MutationObserver(mutations => mutations.forEach(mutation => {
      if (kpwe_cfg.shouldKana(cfg)) {
        if (mutation.type === 'childList') {
          Array.from(mutation.addedNodes)
            .filter(node => node.nodeType === 1 && !iskanaelem(node))
            .map(node => applyKana(cfg, node))
        }
      }
    }))

    // have the observer observe foo for changes in children
    obs.observe(document.body, {
      childList: true,
      subtree: true
    })
  })
}
