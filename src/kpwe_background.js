import kpwe_cfg from './kpwe_cfg.js'
import webapi from './kpwe_webapi.js'

import {
  activetabapplycfg,
  activetabactivated,
  getactivetab
} from './kpwe_activetabmsg.js'

import {
  kpwe_storageget,
  kpwe_storageset
} from './kpwe_storage.js'

const urlhostname = url => {
  const [, , host] = String(url).split('/')

  return (host && host.replace(/[:?]*$/, '')) || null
}

/* c8 ignore start */
const setIcon = cfg => {
  getactivetab((err, tab) => {
    const isorigindisabled = kpwe_cfg
      .isorigindisabled(cfg, urlhostname(tab.url))

    if (/firefox/gi.test(navigator.userAgent)) {
      webapi.webapi.action.setIcon({
        path: 'build/icons/kanaphone-katakana-:iskata-hiragana-:ishira.svg'
          .replace(/:ishira/, !isorigindisabled && cfg.ishiragana)
          .replace(/:iskata/, !isorigindisabled && cfg.iskatakana)
      })
    }
  })
}

webapi.runtime.onMessage.addListener(async (req, sender, send) => {
  if (req.method === 'getcfg') {
    send(await kpwe_storageget())
  } else if (req.method === 'setcfg') {
    send(await kpwe_storageset(req.cfg))
    setIcon(req.cfg)
    activetabapplycfg(req.cfg)
  } else {
    send({})
  }
})

webapi.tabs.onActivated.addListener(async activeInfo => {
  const cfg = await kpwe_storageget()
  setIcon(cfg)
  activetabactivated(cfg, activeInfo.tabId)
})

webapi.tabs.onUpdated.addListener(async (tabId, changeInfo) => {
  if (changeInfo.url) {
    const cfg = await kpwe_storageget()
    setIcon(cfg)
    activetabactivated(cfg, tabId)
  }
})

webapi.webapi.action.onClicked.addListener(async (tab, data) => {
  if (data.button === 1) {
    const cfg = await kpwe_storageget()
    const hostname = urlhostname(tab.url)
    const isorigindisabled = kpwe_cfg
      .isorigindisabled(cfg, hostname)

    if (isorigindisabled) {
      cfg.disabledhostnames = cfg.disabledhostnames
        .filter(disabledhostname => disabledhostname !== hostname)
    } else {
      cfg.disabledhostnames.push(hostname)
    }

    await kpwe_storageset(cfg)
    activetabapplycfg(cfg)
    setIcon(cfg)
  }
})
/* c8 ignore end */

export {
  urlhostname,
  setIcon
}
