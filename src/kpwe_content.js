(async () => await import(
  typeof chrome === 'object'
    ? chrome.runtime.getURL("./build/kpwe.js")
    : browser.runtime.getURL("./build/kpwe.js")))()
