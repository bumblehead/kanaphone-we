export default function nextPart (arr, opts, dofn, fn) {
  if (opts.pos >= arr.length)
    return fn(null, 'success')

  if (opts.activeparts > opts.maxparts)
    return null

  opts.activeparts += 1

  dofn(arr[opts.pos++])

  nextPart(arr, opts, dofn, fn)
  return setTimeout(() => {
    opts.activeparts -= 1
    opts.completedparts += 1

    nextPart(arr, opts, dofn, fn)
  })
}
