const isorigindisabled = (cfg, hostname = window.location.hostname) => (
  (cfg.disabledhostnames || []).some(disabledhostname => (
    disabledhostname === hostname)))

const isKana = cfg => Boolean(cfg.iskatakana || cfg.ishiragana)

const shouldKana = (cfg, hostname = window.location.hostname) => (
  !isorigindisabled(cfg, hostname) && isKana(cfg))

const ischangedfuri = (cfga, cfgb) => (
  cfga.isfurigana !== cfgb.isfurigana)

const ischanged = (cfga, cfgb, hostname = window.location.hostname) => (
  cfga.iskatakana !== cfgb.iskatakana ||
    cfga.ishiragana !== cfgb.ishiragana ||
    cfga.isfurigana !== cfgb.isfurigana ||
    cfga.chardistmax !== cfgb.chardistmax ||
    isorigindisabled(cfga, hostname) !== isorigindisabled(cfgb, hostname))

export default {
  isorigindisabled,
  isKana,
  shouldKana,
  ischangedfuri,
  ischanged
}
