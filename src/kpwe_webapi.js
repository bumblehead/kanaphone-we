/* c8 ignore start */
const ischrome = typeof chrome === 'object'
const ismoz = typeof browser === 'object'

const webapi = ((api = 'unsupported browser') => {
  if (typeof chrome === 'object')
    api = chrome
  else if (typeof browser === 'object')
    api = browser

  return api
})()

const storage = webapi.storage
const tabs = {}
tabs.onActivated = {}
tabs.onActivated.addListener = fn => (
  webapi.tabs.onActivated.addListener(fn))

tabs.onUpdated = {}
tabs.onUpdated.addListener = fn => (
  webapi.tabs.onUpdated.addListener(fn))

if (ischrome) {
  tabs.query = (opts, fn) => (
    webapi.tabs.query(opts, tabs => fn(null, tabs)))

  tabs.sendMessage = (tabid, opts, fn = (() => {})) => (
    webapi.tabs.sendMessage(tabid, opts, res => fn(null, res)))
} else if (ismoz) {
  tabs.query = (opts, fn) => (
    webapi.tabs.query(opts).then(
      tabs => fn(null, tabs),
      err => fn(err)))

  tabs.sendMessage = (tabid, opts, fn = (() => {})) => (
    webapi.tabs.sendMessage(tabid, opts).then(
      tabs => fn(null, tabs),
      err => fn(err)))
}

const runtime = {}
runtime.onMessage = {}
if (ischrome) {
  // https://gist.github.com/akirattii/2f55bb320f414becbc42bbe56313a28b
  runtime.onMessage.addListener = fn => (
    webapi.runtime.onMessage.addListener((req, sender, send) => {
      fn(req, sender, send)

      return true
    }))
  runtime.sendMessage = (opts, fn) => (
    webapi.runtime.sendMessage(opts, res => fn(null, res)))
} else if (ismoz) {
  runtime.onMessage.addListener = fn => (
    webapi.runtime.onMessage.addListener(fn))
  runtime.sendMessage = (opts, fn) => (
    webapi.runtime.sendMessage(opts).then(
      res => fn(null, res),
      err => fn(err)))
}

export default {
  storage,
  tabs,
  runtime,
  webapi,
  ischrome,
  ismoz
}
/* c8 ignore stop */
