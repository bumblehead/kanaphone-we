import {
  getcfg,
  setcfg
} from './kpwe_backgroundmsg.js'

import {
  getactivetab,
  activetablog
}  from './kpwe_activetabmsg.js'

const gEBId = str => document.getElementById(str)

const isdisabledhost = (cfg, hostname) => cfg.disabledhostnames
  .some(disabledhostname => disabledhostname === hostname)

const setkanarange = (cfg, hostname) => {
  const chardistelem = gEBId('chardistmax')
  const chardistmax = chardistelem.max
  const chardistfin = Math.max(chardistmax - cfg.chardistmax, 0)
  const chardistper = Math.round((chardistfin / chardistmax) * 100)

  chardistelem.value = chardistfin
  chardistelem.title = `頻度 ${chardistper}%`
  chardistelem.disabled = isdisabledhost(cfg, hostname)
}

const setKanaButtons = (cfg, hostname) => {
  const isdisabled = isdisabledhost(cfg, hostname);

  ['iskatakana', 'ishiragana', 'isfurigana'].forEach(id => {
    gEBId(id).checked = Boolean(!isdisabled && cfg[id])
    gEBId(id).disabled = isdisabled
  })
}

const listenForClicks = (cfg, tab) => {
  let url = new URL(tab.url),
      { hostname } = url

  setkanarange(cfg, hostname)
  setKanaButtons(cfg, hostname)

  gEBId('isorigindisabled').checked = isdisabledhost(cfg, hostname);

  ['iskatakana', 'ishiragana', 'isfurigana'].forEach(id => {
    gEBId(id).addEventListener('change', e => {
      activetablog(`${id}: ${e.target.checked}`)

      setcfg(Object.assign(cfg, {
        [id]: e.target.checked
      }))

      setKanaButtons(cfg, hostname)
    })
  })
  gEBId('isorigindisabled').addEventListener('change', e => {
    if (e.target.checked) {
      activetablog(`ensabledhostname: ${hostname}`)
      cfg.disabledhostnames.push(hostname)
    } else {
      activetablog(`disabledhostname: ${hostname}`)
      cfg.disabledhostnames = cfg.disabledhostnames
        .filter(disabledhostname => disabledhostname !== hostname)
    }

    setcfg(cfg)
    setkanarange(cfg, hostname)
    setKanaButtons(cfg, hostname)
  })
  gEBId('chardistmax').addEventListener('change', e => {
    activetablog(e.target.value)

    const chardist = e.target.max - Number(e.target.value)

    setcfg(Object.assign(cfg, { chardistmax: chardist }))
    setkanarange(cfg, hostname)
  })
}

getcfg((err, cfg) => {
  getactivetab((err, tab) => {
    listenForClicks(cfg, tab)
  })
})

export {
  setkanarange,
  isdisabledhost,
  setKanaButtons,
  listenForClicks
}
