import kpwe_webapi from './kpwe_webapi.js'

const getactivetab = fn => (
  kpwe_webapi.tabs.query({
    active: true,
    currentWindow: true
  }, (err, tabs) => fn(null, tabs[0])))

const activetablog = msg => (
  getactivetab((err, tab) => (
    kpwe_webapi.tabs.sendMessage(tab.id, {
      method: 'log',
      msg
    })
  )))

const activetabactivated = (cfg, tabid) => (
  kpwe_webapi.tabs.sendMessage(tabid, {
    method: 'activated',
    cfg
  }))

const activetabapplycfg = cfg => (
  getactivetab((err, tab) => (
    kpwe_webapi.tabs.sendMessage(tab.id, {
      method: 'applycfg',
      cfg
    })
  )))

export {
  getactivetab,
  activetablog,
  activetabactivated,
  activetabapplycfg
}
