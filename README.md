kanaphone-we
============

![kanaphone-we][4]

[![pipeline status](https://gitlab.com/bumblehead/kanaphone-we/badges/main/pipeline.svg)](https://gitlab.com/bumblehead/kanaphone-we/pipelines)
[![coverage report](https://gitlab.com/bumblehead/kanaphone-we/badges/main/coverage.svg)](https://gitlab.com/bumblehead/kanaphone-we/commits/main)

Learn kana! Use kanaphone to develop an intuitive feel for hiragana and katakana, while reading english text. Kanaphone updates word-spellings to include hiragana and/or katakana.

_note: browser may need to be restarted after installation._

Move the slider below the katakana and hiragana buttons to increase or decrease the number of kana shown. Click the text below the slider to permanently enable or disable kanaphone for the current webpage, for example, permanently disable kanaphone when checking email... サイト: 有効 is 'sai-tou yuu-kou' or 'site enabled'. Middle-mouse-click the extension icon to shortcut enable or disable the current page.

Kanaphone is available for [firefox][6] and [chrome.][7] It is written by a real person (me) and is just over 600 lines of easily-audited javascript. It needs permission to all browser-accessed urls to allow the following:

 1. [Kanaphone shares data across tabs][10] to auto-update tabs when settings are changed,
 2. [Kanaphone reads and modifies web page text][11] seen by the user and reads urls of user-disabled web pages.
 3. [Kanaphone stores some data][12] in the browser: settings values and a list of user-disabled urls


<img src="screens/kanaphone-we-popout.png" width="280px" />

## contributing

Clone the sources and use `npm start` to build them. _For firefox,_ load the manifest.json in firefox's about:debugging page. _For chrome,_ use `npm run stage` to generate a zip file and load the zip in chrome's chrome://extensions page. For more information about web extensions, start with [this MDN resource.][3]


[0]: http://www.bumblehead.com "bumblehead"
[1]: https://gitlab.com/bumblehead/kanaphone "kanaphone"
[2]: https://chrome.google.com/webstore/detail/easy-kana/nhcjgpmcngkhhfhbnopcjkhnkclhjfme?hl=en "easy-kana"
[3]: https://developer.mozilla.org/en-US/Add-ons/WebExtensions "MDN, WebExtensions"
[4]: src/icons/kanaphone-128x128.png

[5]: screens/screenshot-news.ycombinator.png
[6]: https://addons.mozilla.org/en-US/firefox/addon/kanaphone-we/ "firefox, amo"
[7]: https://chrome.google.com/webstore/detail/kanaphone-we/oppiajkidnbjnoggkjgfligelimopinn?hl=en "chrome"
[8]: https://chrome.google.com/webstore/developer/dashboard?hl=en-US&gl=US "chrome dashboard"


[10]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage
[11]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/tabs
[12]: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/permissions#activeTab_permission
[13]: screens/kanaphone-we-popout.png

![scrounge](https://github.com/iambumblehead/scroungejs/raw/main/img/hand.png)

(The MIT License)

Copyright (c) [Bumblehead][0] <chris@bumblehead.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
